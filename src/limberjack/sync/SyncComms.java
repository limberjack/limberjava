/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

/**
 *
 * @author chaos
 */
public abstract class SyncComms {
    
    public abstract boolean connect(byte[] b);
    
    public abstract boolean disconnect();
    
    public abstract boolean send(byte[] b);
    
}
