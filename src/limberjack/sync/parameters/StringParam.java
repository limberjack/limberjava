/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync.parameters;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chaos
 */
public class StringParam extends Param<String> {

    public StringParam(String b) {
        super();
        value = b;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public void fromString(String s) {
        value = s;
    }

    @Override
    public byte[] toBytes() {
        try {
            return value.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException ex) {
            return value.getBytes();
        }
    }

    @Override
    public void fromBytes(byte[] b) {
        try {
            value = new String(b, "US-ASCII");
        } catch (UnsupportedEncodingException ex) {
            value = new String(b);
        }
    }

    @Override
    public void init() {
        value = "";
    }
    
}
