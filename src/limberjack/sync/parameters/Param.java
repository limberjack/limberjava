/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync.parameters;

/**
 *
 * @author chaos
 */
public abstract class Param<T> {
    
    T value;
    
    public Param() {
        init();
    }
    
    public Param(String s) {
        this.fromString(s);
    }
    
    public Param(byte[] b) {
        this.fromBytes(b);
    }
    
    public abstract String toString();
    
    public abstract void fromString(String s);
    
    public abstract byte[] toBytes();
    
    public abstract void fromBytes(byte[] b);
    
    public abstract void init();
}
