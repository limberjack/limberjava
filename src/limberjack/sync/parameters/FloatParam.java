/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync.parameters;

import java.nio.ByteBuffer;

/**
 *
 * @author chaos
 */
public class FloatParam extends Param<Float> {

    public FloatParam(Float b) {
        super();
        value = b;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public void fromString(String s) {
        value = Float.parseFloat(s);
    }

    @Override
    public byte[] toBytes() {
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putFloat(value);
        return b.array();
    }

    @Override
    public void fromBytes(byte[] b) {
        ByteBuffer bf = ByteBuffer.wrap(b);
        value = bf.getFloat();
    }

    @Override
    public void init() {
        value = 0.0f;
    }
    
}
