/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync.parameters;

import java.nio.ByteBuffer;

/**
 *
 * @author chaos
 */
public class IntegerParam extends Param<Integer> {
    
    public IntegerParam(Integer b) {
        super();
        value = b;
    }
    
    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public void fromString(String s) {
        value = Integer.parseInt(s);
    }

    @Override
    public byte[] toBytes() {
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(value);
        return b.array();
    }

    @Override
    public void fromBytes(byte[] b) {
        ByteBuffer bf = ByteBuffer.wrap(b);
        value = bf.getInt();
    }

    @Override
    public void init() {
        value = 0;
    }
    
}
