/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

/**
 *
 * @author chaos
 */
public abstract class SyncCommsAPI {
    
    public abstract String get(SyncObject s);
    
    public abstract String set(SyncObject s);
    
    public abstract String ack();
    
    public abstract String nack();
    
}
