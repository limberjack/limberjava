/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

/**
 *
 * @author chaos
 */
public class SyncMessage {
    
    public String name;
    public String[] params;
    
    public SyncMessage(String name, String[] params) {
        this.name = name;
        this.params = params;
    }
}
