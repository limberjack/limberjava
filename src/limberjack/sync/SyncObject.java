/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

import limberjack.sync.parameters.IntegerParam;
import limberjack.sync.parameters.StringParam;
import limberjack.sync.parameters.Param;
import limberjack.sync.parameters.FloatParam;
import java.util.ArrayList;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author chaos
 */
public class SyncObject<T extends Param> {

    /**
     * Synchronisation state
     */
    public enum State {
        SYNCED,
        DEVICE_NEEDS_UPDATE,
        CONTROL_NEEDS_UPDATE,
        ERROR
    }
    
    public String name = "default";
    public SyncConfig.Type type = SyncConfig.Type.INTEGER;
    public SyncConfig.Access access = SyncConfig.Access.READONLY;
    public State state = State.CONTROL_NEEDS_UPDATE;
    
    //public ArrayList<SimpleObjectProperty<T>> deviceValues;
    //public ArrayList<SimpleObjectProperty<T>> controlValues;
    
    public SimpleObjectProperty<Param>[] deviceValues;
    public SimpleObjectProperty<Param>[] controlValues;
    
    public SyncObject(SyncConfig s)
    {
        this.name = s.name;
        this.type = s.type;
        this.access = s.access;
        
        //deviceValues = new ArrayList<>();
        //controlValues = new ArrayList<>();
        
        deviceValues = new SimpleObjectProperty[s.numberOfParameters];
        controlValues = new SimpleObjectProperty[s.numberOfParameters];
        
        for (int i = 0; i < s.numberOfParameters; i++) {
            switch (s.type) {
                case INTEGER:
                    deviceValues[i] = new SimpleObjectProperty<>(new IntegerParam(0));
                    controlValues[i] = new SimpleObjectProperty<>(new IntegerParam(0));
                    break;
                case FLOAT:
                    deviceValues[i] = new SimpleObjectProperty<>(new FloatParam(0.0f));
                    controlValues[i] = new SimpleObjectProperty<>(new FloatParam(0.0f));
                    break;
                case STRING:
                    deviceValues[i] = new SimpleObjectProperty<>(new StringParam("default"));
                    controlValues[i] = new SimpleObjectProperty<>(new StringParam("default"));
                    break;
                default:
                    deviceValues[i] = new SimpleObjectProperty<>(new IntegerParam(0));
                    controlValues[i] = new SimpleObjectProperty<>(new IntegerParam(0));
                    break;
            }
            
        }
    }
    
    public static SyncObject FromConfig(SyncConfig s) {
        switch (s.type) {
            case INTEGER:
                return new SyncObject<IntegerParam>(s);
            case FLOAT:
                return new SyncObject<FloatParam>(s);
            case STRING:
                return new SyncObject<StringParam>(s);
            default:
                return new SyncObject<StringParam>(s);
        }
    }
    
    public void setDeviceValues(T[] values) {
        for (int i = 0; i < deviceValues.length; i++) {
            deviceValues[i].set(values[i]);
        }
        updateState();
    }
    
    public void setDeviceValues(String[] values) {
        for (int i = 0; i < deviceValues.length; i++) {
            deviceValues[i].getValue().fromString(values[i]);
        }
        updateState();
    }
    
    public void setDeviceValues(SyncMessage m) {
        setDeviceValues(m.params);
        updateState();
    }
    
    public void setControlValues(T[] values) {
        for (int i = 0; i < controlValues.length; i++) {
            controlValues[i].set(values[i]);
        }
        updateState();
    }
    
    public void setControlValues(String[] values) {
        for (int i = 0; i < controlValues.length; i++) {
            controlValues[i].getValue().fromString(values[i]);
        }
        updateState();
    }
    
    public void setControlValues(SyncMessage m) {
        setControlValues(m.params);
        updateState();
    }
    
    private void updateState() {
        boolean isSynced = true;
        for (int i = 0; i < deviceValues.length; i++) {
            if (deviceValues[i].get() != controlValues[i].get()) {
                isSynced = false;
            }
        }
        if (isSynced) {
            state = State.SYNCED;
        }
    }
}
