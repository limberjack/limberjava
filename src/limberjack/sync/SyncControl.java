/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

import limberjack.sync.parameters.IntegerParam;
import limberjack.sync.parameters.StringParam;
import limberjack.sync.parameters.FloatParam;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;

/**
 *
 * @author chaos
 */
public class SyncControl extends HBox {
    
    //Button buttonName = new Button();
    Label labelName = new Label();
    TextField[] textFieldParams;
    
    public SyncControl(SyncObject s) {
        
        this.spacingProperty().set(10);
        this.alignmentProperty().set(Pos.CENTER_LEFT);
        
        labelName.setText(s.name);
        labelName.setAlignment(Pos.CENTER_RIGHT);
        labelName.prefWidthProperty().set(100);
        this.getChildren().add(labelName);
        
        textFieldParams = new TextField[s.controlValues.length];
        
        int i = 0;
        for (SimpleObjectProperty p : s.controlValues) {
            textFieldParams[i] = new TextField();
            switch (s.type) {
                case INTEGER:
                    textFieldParams[i].textProperty().bindBidirectional(p, integerStringConverter);
                    break;
                case FLOAT:
                    textFieldParams[i].textProperty().bindBidirectional((SimpleObjectProperty<FloatParam>) p, floatStringConverter);
                    break;
                case STRING:
                    textFieldParams[i].textProperty().bindBidirectional((SimpleObjectProperty<StringParam>) p, stringStringConverter);
                    break;
            }
            this.getChildren().add(textFieldParams[i]);
            i++;
        }
        
    }
    
    StringConverter integerStringConverter = new StringConverter() {
        @Override
        public String toString(Object object) {
            return ((IntegerParam) object).toString();
        }
        @Override
        public Object fromString(String string) {
            try {
                return new IntegerParam(Integer.parseInt(string));
            } 
            catch (NumberFormatException ex) {
                return new IntegerParam(0);
            } 
        }
    };
    
    StringConverter stringStringConverter = new StringConverter() {
        @Override
        public String toString(Object object) {
            return ((StringParam) object).toString();
        }
        @Override
        public Object fromString(String string) {
            try {
                return new StringParam(string);
            } 
            catch (NumberFormatException ex) {
                return 0;
            } 
        }
    };
    
    StringConverter floatStringConverter = new StringConverter() {
        @Override
        public String toString(Object object) {
            return ((FloatParam) object).toString();
        }
        @Override
        public Object fromString(String string) {
            try {
                return new FloatParam(Float.parseFloat(string));
            } 
            catch (NumberFormatException ex) {
                return 0;
            } 
        }
    };
    
}
