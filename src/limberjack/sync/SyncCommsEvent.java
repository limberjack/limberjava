/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

/**
 *
 * @author chaos
 */
public class SyncCommsEvent<T> {
    
    public enum Type {
        RX_PACKET,
        RX_MESSAGE,
        ERROR
    }
    public Type eventType;
    public T eventValue;
    
    public SyncCommsEvent(Type t, T o) {
        eventType = t;
        eventValue = o;
    }
    
}
