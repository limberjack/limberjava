/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

/**
 *
 * @author chaos
 */
public interface SyncCommsEventListener {
    
    public abstract void receivePacketEvent(SyncCommsEvent<byte[]> event);
    
    public abstract void errorEvent(SyncCommsEvent<String> event);
    
    public abstract void messageRecievedEvent(SyncCommsEvent<SyncMessage> event);
    
    public abstract void configReceivedEvent(SyncCommsEvent<SyncConfig[]> config);
    
}
