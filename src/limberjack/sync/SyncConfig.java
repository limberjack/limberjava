/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.sync;

/**
 *
 * @author chaos
 */
public class SyncConfig {
    
    public enum Type {
        INTEGER,
        FLOAT,
        STRING
    }
    
    public enum Access {
        READONLY,
        READWRITE,
        FUNCTION
    }
    
    public String name;
    public int numberOfParameters;
    public Number lowerLimit;
    public Number upperLimit;
    public Access access;
    public Type type;
    
    public SyncConfig(String name, Type type, Access access, int params, Number lowerLimit, Number upperLimit) {
        this.name = name;
        this.type = type;
        this.access = access;
        this.numberOfParameters = params;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
    }
    
    public SyncConfig() {
        this.name = "default";
        this.type = Type.INTEGER;
        this.access = Access.READWRITE;
        this.numberOfParameters = 1;
        this.lowerLimit = Integer.MIN_VALUE;
        this.upperLimit = Integer.MAX_VALUE;
    }
    
}
