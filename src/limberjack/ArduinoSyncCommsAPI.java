/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack;

import limberjack.sync.SyncMessage;
import limberjack.sync.SyncCommsAPI;
import limberjack.sync.SyncCommsEvent;
import limberjack.sync.SyncObject;
import limberjack.sync.SyncConfig;
import limberjack.sync.SyncCommsEventListener;
import java.io.CharArrayWriter;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.application.Platform;

/**
 *
 * @author chaos
 */
public class ArduinoSyncCommsAPI extends SyncCommsAPI {

    private StringBuilder rxMessage = new StringBuilder();
    private Boolean isReceivingMessage = false;
    
    private SyncCommsEventListener listener;
    private Boolean eventListenerAdded = false;
    
    /**
     * String to get SyncObject values from device
     * 
     * @param s SyncObject corresponding to device variable
     * @return Get message
     */
    @Override
    public String get(SyncObject s) {
        return "get," + s.name + "\r\n";
    }

    /**
     * String to set SyncObject values on device
     * 
     * @param s SyncObject corresponding to device variable
     * @return Set message
     */
    @Override
    public String set(SyncObject s) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("set,%s", s.name));
        for (Object o : s.deviceValues) {
            sb.append(",");
            sb.append(o.toString());
        }
        sb.append("\r\n");
        return sb.toString();
    }

    /**
     * Acknowledgement message
     * @return Ack string
     */
    @Override
    public String ack() {
        return "ack";
    }

    /**
     * Negative acknowledgement message
     * @return Nack string
     */
    @Override
    public String nack() {
        return "nack";
    }
    
    
    public void process(byte[] buffer) {
        for (byte b : buffer) {
            if (b == '>') {
                isReceivingMessage = true;
                rxMessage.setLength(0);
            }
            else if (isReceivingMessage == true) {
                if (b == '\r') {
                    parse(rxMessage.toString());
                    isReceivingMessage = false;
                }
                else {
                    rxMessage.append((char) b);
                }
            }
        }
    }
    
    public void parse(String s) {
        String[] message = s.split(",");
        
        if (message[0].equals("config")) {
            // Configuration
            ArrayList<SyncConfig> config = new ArrayList<>();
            for (int i = 1; i < message.length + 1 - 6; i++ ) {
                SyncConfig c = new SyncConfig();
                c.name = message[i+0];
                switch (message[i+1]) {
                    case "int": c.type = SyncConfig.Type.INTEGER; break;
                    case "float": c.type = SyncConfig.Type.FLOAT; break;
                    case "string": c.type = SyncConfig.Type.STRING; break;
                }
                switch (message[i+2]) {
                    case "r": c.access = SyncConfig.Access.READONLY; break;
                    case "rw": c.access = SyncConfig.Access.READWRITE; break;
                    case "f": c.access = SyncConfig.Access.FUNCTION; break;
                }
                c.numberOfParameters = Integer.parseInt(message[i+3]);
                switch (c.type) {
                    case INTEGER: 
                        c.lowerLimit = Integer.parseInt(message[i+4]); 
                        c.upperLimit = Integer.parseInt(message[i+5]); 
                        break;
                    case FLOAT: 
                        c.lowerLimit = Float.parseFloat(message[i+4]); 
                        c.upperLimit = Float.parseFloat(message[i+5]); 
                        break;
                    case STRING: 
                        c.lowerLimit = Integer.parseInt(message[i+4]); 
                        c.upperLimit = Integer.parseInt(message[i+5]); 
                        break;
                }
                config.add(c);
            }
            if (eventListenerAdded) listener.configReceivedEvent(
                new SyncCommsEvent<>(
                    SyncCommsEvent.Type.RX_MESSAGE,
                    (SyncConfig[]) config.toArray()
                ));
        }
        else {
            // Normal message
            SyncMessage m  = new SyncMessage(message[0],
                    Arrays.copyOfRange(message, 1,message.length));

            if (eventListenerAdded) listener.messageRecievedEvent(
                new SyncCommsEvent<>(
                    SyncCommsEvent.Type.RX_MESSAGE,
                    m
                ));
        }
    }
    
    public void addEventListener(SyncCommsEventListener l) {
        listener = l;
        eventListenerAdded = true;
    }
    
    public void removeEventListener() {
        eventListenerAdded = false;
    }

    byte[] config() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
