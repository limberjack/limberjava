/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack;

import java.util.ArrayList;
import limberjack.sync.SyncCommsEvent;
import limberjack.sync.SyncCommsEventListener;
import limberjack.sync.SyncConfig;
import limberjack.sync.SyncMessage;
import limberjack.sync.SyncObject;

/**
 *
 * @author chaos
 */
public class SyncManager implements SyncCommsEventListener {
    
    // Sync Objects
    ArrayList<SyncObject> syncObjects = new ArrayList<>();
    SerialSyncComms serialSyncComms = new SerialSyncComms();
    ArduinoSyncCommsAPI syncCommsAPI = new ArduinoSyncCommsAPI();
    
    
    public void begin(String device) {
        serialSyncComms.addEventListener(this);
        syncCommsAPI.addEventListener(this);
        
        if (serialSyncComms.connect(device.getBytes())) {
            serialSyncComms.send(syncCommsAPI.config());
        }
    }
    
    public void end() {
        
    }
    
    private void parseConfig(SyncConfig[] config) {
        syncObjects.clear();
        for (SyncConfig c : config) {
            syncObjects.add(SyncObject.FromConfig(c));
        }
    }

    @Override
    public void errorEvent(SyncCommsEvent<String> event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void messageRecievedEvent(SyncCommsEvent<SyncMessage> event) {
        for (SyncObject s : syncObjects) {
            if (s.name.equals(event.eventValue.name)) s.setDeviceValues(event.eventValue);
        }
    }

    @Override
    public void configReceivedEvent(SyncCommsEvent<SyncConfig[]> config) {
        parseConfig(config.eventValue);
    }

    @Override
    public void receivePacketEvent(SyncCommsEvent<byte[]> event) {
        syncCommsAPI.process(event.eventValue);
    }

}
