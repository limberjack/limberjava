/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.event;

import java.util.ArrayList;

/**
 *
 * @author chaos
 */
public abstract class EventGenerator<T> {
    
    ArrayList<T> listeners = new ArrayList<>();
    
    public synchronized void addListener( T l ) {
        listeners.add( l );
    }
    
    public synchronized void removeListener( T l ) {
        listeners.remove( l );
    }
     
//    private synchronized void _fireMoodEvent() {
//        MoodEvent mood = new MoodEvent( this, _mood );
//        Iterator listeners = _listeners.iterator();
//        while( listeners.hasNext() ) {
//            ( (MoodListener) listeners.next() ).moodReceived( mood );
//        }
//    }
    
}
