/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack.event;

/**
 *
 * @author chaos
 */
public class Event<T> {
    
    Class source;
    T value;
    
    public Event(Class source, T value) {
        this.source = source;
        this.value = value;
    }
}
