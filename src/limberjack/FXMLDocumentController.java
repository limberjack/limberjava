/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import limberjack.sync.SyncConfig;
import limberjack.sync.SyncControl;
import limberjack.sync.parameters.IntegerParam;
import limberjack.sync.SyncObject;
import limberjack.sync.parameters.Param;

/**
 *
 * @author chaos
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private VBox vbc;
    
    SyncObject s;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Button b = new Button("Test");
        vbc.getChildren().add(b);
        
        s = SyncObject.FromConfig(new SyncConfig("int", SyncConfig.Type.INTEGER, SyncConfig.Access.READWRITE, 3, 0, 100));
        s.controlValues[0].set(new IntegerParam(2));
        s.controlValues[1].set(new IntegerParam(3));
        s.controlValues[2].set(new IntegerParam(4));
        SyncControl c = new SyncControl(s);
        
        vbc.getChildren().add(c);
        vbc.getChildren().add(new SyncControl(SyncObject.FromConfig(new SyncConfig("float", SyncConfig.Type.FLOAT, SyncConfig.Access.READWRITE, 2, 0, 100))));
        vbc.getChildren().add(new SyncControl(SyncObject.FromConfig(new SyncConfig("string", SyncConfig.Type.STRING, SyncConfig.Access.READWRITE, 1, 0, 100))));
        
    }    
    
}
