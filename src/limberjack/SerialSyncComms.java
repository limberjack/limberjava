/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package limberjack;

import limberjack.sync.SyncCommsEvent;
import limberjack.sync.SyncComms;
import limberjack.sync.SyncCommsEventListener;
import javafx.application.Platform;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 *
 * @author chaos
 */
public class SerialSyncComms extends SyncComms implements SerialPortEventListener  {
    
    // Error - convert to SyncCommsEvent
    public String errorString;
    
    // Communication events
    SyncCommsEventListener listener;
    boolean eventListenerAdded = false;
    
    //Serial port
    private SerialPort serialPort;

    @Override
    public boolean connect(byte[] b) {
        try {
            serialPort = new SerialPort(b.toString());
            // Close port if opened
            if (serialPort.isOpened()) {
                serialPort.closePort();
            }
            serialPort.openPort();        
            serialPort.setParams(SerialPort.BAUDRATE_57600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            serialPort.setEventsMask(SerialPort.MASK_RXCHAR);
            serialPort.addEventListener(this);
        } 
        catch (SerialPortException ex) {
            if (eventListenerAdded) listener.errorEvent(
                new SyncCommsEvent<>(
                        SyncCommsEvent.Type.ERROR, ex.getMessage()));
            errorString = ex.getMessage();
            return false;
        }
        return true;
    }

    @Override
    public boolean disconnect() {
        try {
            if (serialPort != null) {
                serialPort.closePort();
            }
        } catch (SerialPortException ex) {
            errorString = ex.getMessage();
            if (eventListenerAdded) listener.errorEvent(
                new SyncCommsEvent<>(
                        SyncCommsEvent.Type.ERROR, ex.getMessage()));
            return false;
        }
        return true;
    }

    @Override
    public boolean send(byte[] b) {
        try {
            if (serialPort.isOpened()) {
                    serialPort.writeBytes(b);
            }
        } catch (SerialPortException ex) {
            if (eventListenerAdded) listener.errorEvent(
                new SyncCommsEvent<>(
                        SyncCommsEvent.Type.ERROR, ex.getMessage()));
            errorString = ex.getMessage();
            return false;
        }
        return true;
    }
    
    public void receive(byte[] b) {
        if (eventListenerAdded) listener.receivePacketEvent(
                new SyncCommsEvent<>(
                        SyncCommsEvent.Type.RX_PACKET, b));
    }
    
    public void addEventListener(SyncCommsEventListener l) {
        listener = l;
        eventListenerAdded = true;
    }
    
    public void removeEventListener() {
        eventListenerAdded = false;
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        if(event.isRXCHAR() && event.getEventValue() > 0) {//If data is available
            try {
                byte buffer[] = serialPort.readBytes();
                Platform.runLater(new Runnable() {
                    @Override 
                    public void run() {
                        receive(buffer);
                    }
                });
            }
            catch (SerialPortException ex) {
                errorString = ex.getMessage();
            }
        }
    }
}
