LimberJava
==========

Java program for custom serial control of Arduino boards.






Classes
=======

** Rough Draft **

* Notes: *
* Might be too many classes.

SyncObject
----------
Contains two sets of variables, device (Arduino state) and control (PC) state. Getters and setters for each variable plus checking to see if they are synced. Can be readonly (e.g. sensor reading), readwrite (e.g. setting) or writeonly (e.g. function)


SyncCommunicationsAPI
------------------
Abstract class that defines communicaton protocol.

* Make packet from SyncObject
* Parse packet into generic parameters
* Define behaviour - ack, nack etc.
* Define errors - bad formatting, checksum, etc.

Example implementations:
* SerialSyncCommunicationsAPI - Human readable messages, e.g. "set,variableName,param1,param2\r\n"
* XBeeSyncCommunicationsAPI - XBee binary message format
* etc.


SyncCommunications
------------------
Abstract class that defines communication channel. Has methods to enumerate devices, open and close connections, send messages and wait for replies.

Example implementatons:
* SerialSyncCommunications - COM ports
* NetworkSyncCommunications - IP Address
* RestfulSyncCommunication - HTTP REST interface


SyncManager
-----------
Ties all the comms together. List of SyncObjects plus SyncCommunicatons and SyncCommunicatonsAPI. High level logic that handles message queing, updating SyncObjects, erros, etc.


SyncControl
-----------
Automatically generated control from SyncObject. 


SyncController
--------------
Glues SyncControls to SyncManager


